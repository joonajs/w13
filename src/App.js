import './App.css';

function App() {
  const weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
  const rowsOfDays = weekdays.map((day, position) => (
    <tr key={position}>
      <td>{day}</td>
    </tr>
  ));

  return (
    <div className="App">
      <h1 className='W13'>W13 Weekdays in a table</h1>
      <table>
        <thead>
          <tr> 
            <th>Weekday</th>
          </tr>
        </thead>
        <tbody>
          {rowsOfDays}
        </tbody>
      </table>
    </div>
  );
}

export default App;
